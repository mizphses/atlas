Rails.application.routes.draw do
  get 'admin/settings'
  get 'admin/' => 'admin#index'
  get 'admin/new'
  post 'admin/submits'
  post 'admin/submitc'
  get 'admin/:id' => 'admin#edit'
  patch 'admin/:id/update' => 'admin#update'
  delete 'admin/d/:id' => 'admin#destroy'
  patch 'service/edit/:id/' => 'service#update'
  delete 'service/d/:id' => 'service#destroy'
  get 'service/index'
  get 'service/:id' => 'service#la'
  post 'service/submit_la'
  get 'dash' => 'service#dash'
  get 'edit/:id' => 'service#edit'
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  root to: "home#index"
  get 'index' => 'home#index'
  get '/' => 'home#index'
  get 'about' => 'home#about'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
