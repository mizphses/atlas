# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(username: "管理者", email: "admin@example.com", password: "atlas2020", role: "9")
Apptype.create(name: "発注（食材系）", description: "食材の発注関係はこちらへ", area: "1", user_mail: "admin@example.com")
Apptype.create(name: "発注（資材系）", description: "資材の発注関係はこちらへ", area: "1", user_mail: "admin@example.com")
Apptype.create(name: "経費精算", description: "事後精算承認はこちらへ", area: "2", user_mail: "admin@example.com")