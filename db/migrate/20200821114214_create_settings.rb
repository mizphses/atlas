class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.string :app_name
      t.string :description
      t.string :company_name
      t.boolean :create_user, default: true, null: false
      t.integer :new_user_policy

      t.timestamps
    end
  end
end
