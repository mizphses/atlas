class CreateApptypes < ActiveRecord::Migration[6.0]
  def change
    create_table :apptypes do |t|
      t.string :name
      t.string :description
      t.string :area
      t.string :user_mail

      t.timestamps
    end
  end
end
