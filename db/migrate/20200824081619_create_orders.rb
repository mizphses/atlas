class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :apprication_id
      t.string :user_email
      t.integer :odtype
      t.string :whatto
      t.string :description
      t.string :upload_file_name
      t.binary :upload_file
      t.integer :status
      t.integer :approve
      t.string :apr_reason
      t.string :provider

      t.timestamps
    end
  end
end
