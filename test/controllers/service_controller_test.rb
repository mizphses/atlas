require 'test_helper'

class ServiceControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get service_index_url
    assert_response :success
  end

  test "should get la" do
    get service_la_url
    assert_response :success
  end

  test "should get sb" do
    get service_sb_url
    assert_response :success
  end

  test "should get co" do
    get service_co_url
    assert_response :success
  end

  test "should get confirm" do
    get service_confirm_url
    assert_response :success
  end

end
