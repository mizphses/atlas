require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  test "should get settings" do
    get admin_settings_url
    assert_response :success
  end

  test "should get index" do
    get admin_index_url
    assert_response :success
  end

  test "should get new" do
    get admin_new_url
    assert_response :success
  end

  test "should get submits" do
    get admin_submits_url
    assert_response :success
  end

  test "should get submitc" do
    get admin_submitc_url
    assert_response :success
  end

end
