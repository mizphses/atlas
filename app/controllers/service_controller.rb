class ServiceController < ApplicationController
  def index
  end

  def la
    @atypes = Apptype.find(params[:id])
    @order = Order.new
  end

  def submit_la

    @order = Order.new(order_params)

    # インスタンスの保存に成功した場合の処理
    if @order.save
      flash[:success] = "情報を登録しました"
      redirect_to "/"

    # インスタンスの保存に失敗した場合の処理
    else
      flash[:danger] = "情報の登録に失敗しました"
      render :la
    end
  end

  def dash
    @order = Order.all
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(updater)
      flash[:success] = "発注情報を更新しました"
      redirect_to("/editor/"+params[:id])
    else
      flash[:danger] = "発注情報の更新に失敗しました"
      render :edit
    end
  end
  
    def destroy
      @order = Apptype.find(params[:id])
      if @order.destroy
        flash[:success] = "削除しました"
        redirect_to("/admin/")
      else
        flash[:danger] = "削除できませんでした"
        render :index
      end
    end

  private

  def order_params
    params.require(:order).permit(:odtype, :whatto, :description, :upload_file, :user_email)
  end
  def updater
    params.require(:order).permit(:odtype, :whatto, :description, :upload_file, :user_email, :approve, :apr_reason, :provider)
  end

end
