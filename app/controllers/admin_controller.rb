class AdminController < ApplicationController
  def settings
  end

  def index
    @apptype = Apptype.all.order(id: "ASC")
  end

  def new
    @apptype = Apptype.new
  end

  def edit
    @apptype = Apptype.find_by(id: params[:id])
  end

  def update
    @apptype = Apptype.find(params[:id])
    if @apptype.update(apptype_params)
      flash[:success] = "発注情報を更新しました"
      redirect_to("/admin/"+params[:id])
    else
      flash[:danger] = "発注情報の更新に失敗しました"
      render :edit
    end

  end

  def destroy
    @apptype = Apptype.find(params[:id])
    if @apptype.destroy
      flash[:success] = "削除しました"
      redirect_to("/admin/")
    else
      flash[:danger] = "削除できませんでした"
      render :index
    end
  end

  def submits
  end

  def submitc
    @apptype = Apptype.new(apptype_params)

    # インスタンスの保存に成功した場合の処理
    if @apptype.save
      flash[:success] = "発注情報を登録しました"
      redirect_to "/admin/new"

    # インスタンスの保存に失敗した場合の処理
    else
      flash[:danger] = "発注情報の登録に失敗しました"
      render :new
    end
  end

  private

  def apptype_params
    params.require(:apptype).permit(:name, :description, :area, :user_mail)
  end

end
