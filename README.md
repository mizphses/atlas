# Atlas Approval Manager
経費・支出承認とかができるOSSアプリです。普通のRailsアプリと同様にProduction組んでください。自前でやるならNginxでやることをお勧めします。（処理落ちリスクがあって危ないので）

## Masterブランチについて
即時デプロイの対象になってしまうので緊急時以外は直接pushは禁止します。devとか任意のブランチを立ててください。送り先はdevでお願いします。

## QAコーナー（質問は随時追加してください。）
### Q.プログラム直したい
GitLab登録してFork→Merge Request打ってください。わからない場合・できない場合はIssueかメールください。（最近GitHubから移行したので詳しくないです）

### Q.サーバー持ってない
ここのコードそのまま持ってってアプリ名（atlas-order）を直して変数（HEROKU_PRODUCTION_API_KEY）を設定したらHerokuに自動デプロイできます。学生さんならGitHub Backpackで無料で結構使えるので登録すべし。 ```Heroku Rails GitLab CI``` で検索してください。

### Q.フォントって何使ってんの
フロップデザインのスマートフォントUI、RSMSのInter、Google/AdobeのNoto Sans JPです。全部OSSフォントです。

### Q.バンドルされてる画像とかコード以外の版権教えて
画像はコードではないのでMITには入りませんが、同様に再利用を許可します。フォントは各社再頒布の方針に従ってください。（SIL OFLとIPAフォントライセンスをご確認ください）

### Q.コードの意味がわからん
Ruby/ Ruby on Railsを勉強してください。

### Q.私の権利が侵害されている
Issueください。

### Q.初期ID・パスワードは？
管理者No.1はseeds.rbで定義してます。

```
  ID: admin@example.com
  パスワード: atlas2020
  名前: 管理者
```
### Q.APIはないのか
面倒かつ忙しいので作ってません。Postgresqlで管理してるのでExpressとかから繋ぐコード作ってもいいんだよ。とりあえず誰か作って。

### Q.ヘルプくれ
基本「このアプリについて」と「経費申請の流れ」に書いてる。他で欲しい部分はIssue打って。

### Q. Can I make some issues here in English?（外国語可能か）
Only Japanese and English are accepted. No Chinese or else. 中文不能使用。日本語と英語のみ。中国語はダメ。
